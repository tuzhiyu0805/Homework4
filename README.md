# 2020-2021学年第一学期软件技术基础第四次作业

#### 介绍
这是2020-2021学年第一学期软件技术基础第四次作业

#### 作业要求
实现一个简易命令行计算器程序。输入数字和算法后能进行四则运算加减乘除，计算结果保留到小数点后2位。
程序要求能处理用户的输入，判断异常。
程序支持可以由用户自行选择加、减、乘、除运算。

#### 版本信息
V1.0 

     基础功能

V1.1 

     增加了提示语，提升用户端体验；

     修复了除数为0时未提示报错而显示两数相加的结果的BUG；

     现在加法得到的结果也是保留两位小数了。

#### 使用方法
直接输入您想要计算的算式，单击回车即可查看结果


#### 结果截图
V1.0版本 结果截图

![1.0版本 加法](https://images.gitee.com/uploads/images/2021/1121/151415_23dbd41e_9720354.jpeg "1.0版本 加法.jpg")

![1.0版本 减法](https://images.gitee.com/uploads/images/2021/1121/151437_fa2faf90_9720354.jpeg "1.0版本 减法.jpg")

![1.0版本 乘法](https://images.gitee.com/uploads/images/2021/1121/151451_89a73939_9720354.jpeg "1.0版本 乘法.jpg")

![1.0版本 除法](https://images.gitee.com/uploads/images/2021/1121/151504_7d172bb2_9720354.jpeg "1.0版本 除法.jpg")



V1.1版本 结果截图

![1.1版本 加法](https://images.gitee.com/uploads/images/2021/1121/152617_b79b5b27_9720354.jpeg "1.1版本 加法.jpg")

![1.1版本 减法](https://images.gitee.com/uploads/images/2021/1121/152628_71a2b52e_9720354.jpeg "1.1版本 减法.jpg")

![1.1版本 乘法](https://images.gitee.com/uploads/images/2021/1121/152641_8ee50003_9720354.jpeg "1.1版本 乘法.jpg")

![1.1版本 除法](https://images.gitee.com/uploads/images/2021/1121/152655_eaafdcb3_9720354.jpeg "1.1版本 除法.jpg")

![1.1版本 异常符号报错](https://images.gitee.com/uploads/images/2021/1121/152709_e07b28a9_9720354.jpeg "1.1版本 报错.jpg")

![1.1版本 除数为0报错](https://images.gitee.com/uploads/images/2021/1121/152726_b6a48568_9720354.jpeg "1.1版本 除数为0.jpg")

